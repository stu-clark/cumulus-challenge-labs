### Cumulus Challenge Labs
<!-- AIR:tour -->
These labs are provided as a next step in developing troubleshooting skills on Cumulus Linux.

#### Setup
Change directory into the `cumulus-challange-labs` directory on the oob-mgmt-server:
```
cumulus@oob-mgmt-server:~$ cd cumulus-challenge-labs
cumulus@oob-mgmt-server:~/cumulus-challenge-labs$
```

The included `run` script is used to load the challenges, as well as help validate solutions for each lab:
```
cumulus@oob-mgmt-server:~/cumulus-challenge-labs$ ./run -h
usage: ./run [-c <challenge> -a [load|validate] | -h]
```

To load a challenge, execute the `run` script with the `load` action. For example, to load the first challenge lab:
```
cumulus@oob-mgmt-server:~/cumulus-challenge-labs$ ./run -c 1 -a load
```

Once the script completes, the configurations for the first challenge lab will be loaded on all the devices. Challenge specific information, including the topology, hints and solution, are included in the lab specific sections below.

#### Validate
After a solution has been found for a given challenge, the run script provides a basic connectivity check to help validate the solution:
```
cumulus@oob-mgmt-server:~/cumulus-challenge-labs$ ./run -c 1 -a validate
```
<!-- AIR:page -->

### Challenge List
[Challenge 1](#challenge-1)\
[Challenge 2](#challenge-2)\
[Challenge 3](#challenge-3)\
[Challenge 4](#challenge-4)
<!-- AIR:page -->

### Challenge 1
#### Setup Command
```
./run -c 1 -a load
```

#### Validation Command
```
./run -c 1 -a validate
```

#### Topology Diagram
![topology](https://gitlab.com/cumulus-consulting/education/cumulus-challenge-labs/-/raw/trunk/challenges/1/topology.png)

#### Problem Statement
Server01 is unable to ping server02 or server03. Server02 and server03 are able to ping each other.

Try to identify the problem in the network leading to the connectivity issues.

***Continue to the next page for hints to help spot the problem.***
<!-- AIR:page -->

#### Hint 1
Check the MAC tables on the leaf switches:
  
```
cumulus@leaf01:mgmt-vrf:~$ net show bridge macs

VLAN      Master  Interface  MAC                TunnelDest  State      Flags  LastSeen
--------  ------  ---------  -----------------  ----------  ---------  -----  --------
1         bridge  swp1       c0:c7:61:c5:cf:e4                                00:00:02
1         bridge  swp49      62:1c:11:bb:ee:8e                                00:18:50
1         bridge  swp49      aa:38:32:4a:87:bf                                00:14:20
1         bridge  swp49      b4:cd:b7:e3:aa:af                                00:00:18
10        bridge  swp49      aa:38:32:4a:87:bf                                00:14:06
untagged  bridge  swp1       3c:f9:30:a1:5a:2a              permanent         00:19:06
untagged  bridge  swp49      c0:5c:a4:3e:d4:55              permanent         00:19:06
```

```
cumulus@leaf02:mgmt-vrf:~$ net show bridge macs

VLAN      Master  Interface  MAC                TunnelDest  State      Flags  LastSeen
--------  ------  ---------  -----------------  ----------  ---------  -----  --------
1         bridge  swp49      c0:5c:a4:3e:d4:55                                00:00:01
1         bridge  swp49      c0:c7:61:c5:cf:e4                                00:16:02
10        bridge  swp2       aa:38:32:4a:87:bf                                00:00:03
10        bridge  swp3       62:1c:11:bb:ee:8e                                00:00:03
untagged  bridge  swp2       f0:99:1b:66:dc:15              permanent         00:21:08
untagged  bridge  swp3       34:14:b2:5a:20:c8              permanent         00:21:08
untagged  bridge  swp49      b4:cd:b7:e3:aa:af              permanent         00:21:08
```
<!-- AIR:page -->
#### Hint 2
Check the VLAN configurations on the leaf switches:
```
cumulus@leaf01:mgmt-vrf:~$ net show bridge vlan

Interface  VLAN  Flags
---------  ----  ---------------------
swp1          1  PVID, Egress Untagged
             10
swp49         1  PVID, Egress Untagged
             10
```

```
cumulus@leaf02:mgmt-vrf:~$ net show bridge vlan

Interface  VLAN  Flags
---------  ----  ---------------------
swp2         10  PVID, Egress Untagged
swp3         10  PVID, Egress Untagged
swp49         1  PVID, Egress Untagged
             10
```
<!-- AIR:page -->
#### Hint 3
Check configurations of the server facing interfaces on the leaf switches:

```
cumulus@leaf01:mgmt-vrf:~$ ifquery -r swp1
auto swp1
iface swp1
        bridge-vids 10
```
```
cumulus@leaf02:mgmt-vrf:~$ ifquery -r swp2
auto swp2
iface swp2
        bridge-access 10
```

***When ready, continue to the next page to see the solution for this challenge.***
<!-- AIR:page -->

#### Solution
Interface swp1 on leaf01 is configured as a trunk port with VLAN 10 tagged. This interface should be converted to an access port:

```
cumulus@leaf01:mgmt-vrf:~$ net add int swp1 bridge access 10
cumulus@leaf01:mgmt-vrf:~$ net commit
```
***Documentation**: [https://docs.cumulusnetworks.com/cumulus-linux-37/Layer-2/Ethernet-Bridging-VLANs/VLAN-aware-Bridge-Mode/#untaggedaccess-ports](https://docs.cumulusnetworks.com/cumulus-linux-37/Layer-2/Ethernet-Bridging-VLANs/VLAN-aware-Bridge-Mode/#untaggedaccess-ports)*

*[Continue to next challenge](#challenge-2) or [Back to Lab Overview](#challenge-list)*
<!-- AIR:page -->

### Challenge 2
#### Setup Command
```
./run -c 2 -a load
```

#### Validation Command
```
./run -c 2 -a validate
```

#### Topology Diagram
![topology](https://gitlab.com/cumulus-consulting/education/cumulus-challenge-labs/-/raw/trunk/challenges/2/topology.png)

#### Problem Statement
Server02 is unable to reach server01 or server03. Connectivity between server01 and server03 is fine.

Try to identify the problem in the network leading to the connectivity issues.

***Continue to the next page for hints to help spot the problem.***
<!-- AIR:page -->

#### Hint 1
Check the routing table on the spine switches to confirm the leaf switch prefixes are learned:
```
cumulus@spine01:mgmt-vrf:~$ net show route bgp
RIB entry for bgp
=================
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route

B>* 0.0.0.0/0 [200/0] unreachable (blackhole), 00:07:26
B>* 10.1.1.0/24 [20/0] via fe80::a4dc:bcff:fea5:fe28, swp1, 00:07:22
B>* 10.1.2.0/24 [20/0] via fe80::da09:bfff:fe75:f048, swp2, 00:07:22
B>* 10.1.4.0/24 [20/0] via fe80::4630:f3ff:fe1c:9639, swp4, 00:07:22
B>* 10.10.10.1/32 [20/0] via fe80::a4dc:bcff:fea5:fe28, swp1, 00:07:22
B>* 10.10.10.2/32 [20/0] via fe80::da09:bfff:fe75:f048, swp2, 00:07:22
B>* 10.10.10.4/32 [20/0] via fe80::4630:f3ff:fe1c:9639, swp4, 00:07:22
B>* 192.168.200.0/24 [20/0] via fe80::da09:bfff:fe75:f048, swp2, 00:07:22 
```
<!-- AIR:page -->

#### Hint 2
Check the routing table on the leaf switches and confirm the default route is present:
```
cumulus@leaf01:mgmt-vrf:~$ net show route bgp
RIB entry for bgp
=================
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route

B>* 0.0.0.0/0 [20/0] via fe80::885:d9ff:feb7:63ba, swp52, 00:11:16
  *                  via fe80::cae:46ff:fe3d:f8d2, swp51, 00:11:16
```
<!-- AIR:page -->

#### Hint 3
The BGP learned default route is not installed on leaf02:
```
cumulus@leaf02:~$ net show route bgp
RIB entry for bgp
=================
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route

B   0.0.0.0/0 [20/0] via fe80::3cfb:56ff:feb7:e805, swp51, 00:12:44
                     via fe80::78bc:64ff:fee2:48a2, swp52, 00:12:44
```
<!-- AIR:page -->

#### Hint 4
There is a default route via eth0 in the routing table on leaf02:
```
cumulus@leaf02:~$ net show route
show ip route
=============
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route

B   0.0.0.0/0 [20/0] via fe80::3cfb:56ff:feb7:e805, swp51, 00:14:00
                     via fe80::78bc:64ff:fee2:48a2, swp52, 00:14:00
K>* 0.0.0.0/0 [0/0] via 192.168.200.1, eth0, 00:14:04
C>* 10.1.2.0/24 is directly connected, vlan2, 00:14:03
C>* 10.10.10.2/32 is directly connected, lo, 00:14:04
C>* 192.168.200.0/24 is directly connected, eth0, 00:14:04



show ipv6 route
===============
Codes: K - kernel route, C - connected, S - static, R - RIPng,
       O - OSPFv3, I - IS-IS, B - BGP, N - NHRP, T - Table,
       v - VNC, V - VNC-Direct, A - Babel, D - SHARP, F - PBR,
       > - selected route, * - FIB route

C * fe80::/64 is directly connected, bridge, 00:14:01
C * fe80::/64 is directly connected, vlan2, 00:14:02
C * fe80::/64 is directly connected, swp52, 00:14:03
C * fe80::/64 is directly connected, swp51, 00:14:03
C>* fe80::/64 is directly connected, eth0, 00:14:03
```
<!-- AIR:page -->

#### Hint 5
Compare the prompt of leaf01 and leaf02:
```
cumulus@leaf01:mgmt-vrf:~$
```
```
cumulus@leaf02:~$
```

***When ready, continue to the next page to see the solution for this challenge.***
<!-- AIR:page -->

#### Solution
The managment VRF is not configured on leaf02:

```
cumulus@leaf02:~$ net add vrf mgmt

*********************************************************************************
NOTE: Enabling or disabling Management VRF will cause all SSH sessions to
disconnect on the next "net commit". This only happens the first time you do
a "net commit" after enabling or disabling Management VRF.

Enabling or disabling Management VRF may interfere with other previously
configured services which may previously have been using the management interface
for communication including: NTP, DNS, API, CLAG Backup IP, SNMP. See
"net example management-vrf" for more practical examples on using Management VRF
with NCLU.
*********************************************************************************
cumulus@leaf02:~$ net commit
Connection to leaf02 closed by remote host.
Connection to leaf02 closed.
```
After enabling mgmt VRF on leaf02, re-connect via SSH and verify the routing table now shows the correct default, and the default via eth0 is now present  the mgmt VRF:
```
cumulus@leaf02:mgmt-vrf:~$ net show route 
show ip route
=============
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route

B>* 0.0.0.0/0 [20/0] via fe80::3cfb:56ff:feb7:e805, swp51, 00:21:19
  *                  via fe80::78bc:64ff:fee2:48a2, swp52, 00:21:19
C>* 10.1.2.0/24 is directly connected, vlan2, 00:21:22
C>* 10.10.10.2/32 is directly connected, lo, 00:21:23
```

```
cumulus@leaf02:mgmt-vrf:~$ net show route vrf mgmt
show ip route vrf mgmt 
=======================
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route


VRF mgmt:
K>* 0.0.0.0/0 [0/0] via 192.168.200.1, eth0, 00:04:19
K * 0.0.0.0/0 [255/8192] unreachable (ICMP unreachable), 00:04:21
C>* 192.168.200.0/24 is directly connected, eth0, 00:04:20
```

***Documentation**: [https://docs.cumulusnetworks.com/cumulus-linux-37/Layer-3/Management-VRF/](https://docs.cumulusnetworks.com/cumulus-linux-37/Layer-3/Management-VRF/)*

*[Continue to next challenge](#challenge-3) or [Back to Lab Overview](#challenge-list)*
<!-- AIR:page -->

### Challenge 3
#### Setup Command
```
./run -c 3 -a load
```

#### Validation Command
```
./run -c 3 -a validate
```

#### Topology
![topology](https://gitlab.com/cumulus-consulting/education/cumulus-challenge-labs/-/raw/trunk/challenges/3/topology.png)

#### Problem Statement
The servers attached to leaf01 and leaf02 are able to communicate with each other. The server attached to leaf04 is unable to communicate with servers behind leaf01 or leaf02.

Try to identify the problem in the network leading to the connectivity issues.

***Continue to the next page for hints to help spot the problem.***
<!-- AIR:page -->

#### Hint 1
Check the neighbor entries on the servers:
```
cumulus@server01:~$ ip neigh show
192.168.200.2 dev eth0 lladdr c6:18:63:4d:4e:a1 STALE
192.168.200.1 dev eth0 lladdr f4:92:37:80:89:4e REACHABLE
192.168.200.250 dev eth0 lladdr 44:38:39:00:01:80 REACHABLE
10.1.123.102 dev eth1 lladdr aa:38:32:4a:87:bf STALE
10.1.123.104 dev eth1  FAILED
fe80::c418:63ff:fe4d:4ea1 dev eth0 lladdr c6:18:63:4d:4e:a1 router STALE
```
<!-- AIR:page -->

#### Hint 2
Check the MAC table on the leaf switches for the MAC of server04:
```
cumulus@leaf01:mgmt-vrf:~$ net show bridge macs 2a:a6:4a:54:01:7e

VLAN  Master  Interface  MAC  TunnelDest  State  Flags  LastSeen
----  ------  ---------  ---  ----------  -----  -----  --------
```
```
cumulus@leaf04:mgmt-vrf:~$ net show bridge macs 2a:a6:4a:54:01:7e

VLAN  Master  Interface  MAC                TunnelDest  State  Flags  LastSeen
----  ------  ---------  -----------------  ----------  -----  -----  --------
 123  bridge  swp1       2a:a6:4a:54:01:7e                            00:00:18
```
<!-- AIR:page -->

#### Hint 3
Check the EVPN type 2 routes on the leaf switches:
```
cumulus@leaf01:mgmt-vrf:~$ net show bgp l2vpn evpn route type macip
BGP table version is 2, local router ID is 10.10.10.1
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal
Origin codes: i - IGP, e - EGP, ? - incomplete
EVPN type-2 prefix: [2]:[ESI]:[EthTag]:[MAClen]:[MAC]:[IPlen]:[IP]
EVPN type-3 prefix: [3]:[EthTag]:[IPlen]:[OrigIP]
EVPN type-5 prefix: [5]:[ESI]:[EthTag]:[IPlen]:[IP]

   Network          Next Hop            Metric LocPrf Weight Path
                    Extended Community
Route Distinguisher: 10.10.10.1:2
*> [2]:[0]:[0]:[48]:[c0:c7:61:c5:cf:e4]
                    10.10.10.1                         32768 i
                    ET:8 RT:65001:100123
Route Distinguisher: 10.10.10.2:2
*  [2]:[0]:[0]:[48]:[aa:38:32:4a:87:bf]
                    10.10.10.2                             0 65000 65002 i
                    RT:65002:100123 ET:8
*> [2]:[0]:[0]:[48]:[aa:38:32:4a:87:bf]
                    10.10.10.2                             0 65000 65002 i
                    RT:65002:100123 ET:8
Route Distinguisher: 10.10.10.4:2
*> [2]:[0]:[0]:[48]:[2a:a6:4a:54:01:7e]
                    10.10.10.4                             0 65000 65004 i
                    RT:65004:123 ET:8
*  [2]:[0]:[0]:[48]:[2a:a6:4a:54:01:7e]
                    10.10.10.4                             0 65000 65004 i
                    RT:65004:123 ET:8

Displayed 3 prefixes (5 paths) (of requested type)
```

```
cumulus@leaf04:mgmt-vrf:~$ net show bgp l2vpn evpn route type macip
BGP table version is 2, local router ID is 10.10.10.4
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal
Origin codes: i - IGP, e - EGP, ? - incomplete
EVPN type-2 prefix: [2]:[ESI]:[EthTag]:[MAClen]:[MAC]:[IPlen]:[IP]
EVPN type-3 prefix: [3]:[EthTag]:[IPlen]:[OrigIP]
EVPN type-5 prefix: [5]:[ESI]:[EthTag]:[IPlen]:[IP]

   Network          Next Hop            Metric LocPrf Weight Path
                    Extended Community
Route Distinguisher: 10.10.10.1:2
*  [2]:[0]:[0]:[48]:[c0:c7:61:c5:cf:e4]
                    10.10.10.1                             0 65000 65001 i
                    RT:65001:100123 ET:8
*> [2]:[0]:[0]:[48]:[c0:c7:61:c5:cf:e4]
                    10.10.10.1                             0 65000 65001 i
                    RT:65001:100123 ET:8
Route Distinguisher: 10.10.10.2:2
*  [2]:[0]:[0]:[48]:[aa:38:32:4a:87:bf]
                    10.10.10.2                             0 65000 65002 i
                    RT:65002:100123 ET:8
*> [2]:[0]:[0]:[48]:[aa:38:32:4a:87:bf]
                    10.10.10.2                             0 65000 65002 i
                    RT:65002:100123 ET:8
Route Distinguisher: 10.10.10.4:2
*> [2]:[0]:[0]:[48]:[2a:a6:4a:54:01:7e]
                    10.10.10.4                         32768 i
                    ET:8 RT:65004:123

Displayed 3 prefixes (5 paths) (of requested type)
```
<!-- AIR:page -->

#### Hint 4
Check the VNI information on the leaf switches:
```
cumulus@leaf01:mgmt-vrf:~$ net show bgp l2vpn evpn vni
Advertise Gateway Macip: Disabled
Advertise SVI Macip: Disabled
Advertise All VNI flag: Enabled
BUM flooding: Head-end replication
Number of L2 VNIs: 1
Number of L3 VNIs: 0
Flags: * - Kernel
  VNI        Type RD                    Import RT                 Export RT                 Tenant VRF                           
* 100123     L2   10.10.10.1:2          65001:100123              65001:100123             default
```
```
cumulus@leaf04:mgmt-vrf:~$ net show bgp l2vpn evpn vni
Advertise Gateway Macip: Disabled
Advertise SVI Macip: Disabled
Advertise All VNI flag: Enabled
BUM flooding: Head-end replication
Number of L2 VNIs: 1
Number of L3 VNIs: 0
Flags: * - Kernel
  VNI        Type RD                    Import RT                 Export RT                 Tenant VRF                           
* 123        L2   10.10.10.4:2          65004:123                 65004:123                default
```
<!-- AIR:page -->

#### Hint 5
Compare the VNI configurations on the leaf switches:
```
cumulus@leaf01:mgmt-vrf:~$ ifquery vni123
auto vni123
iface vni123
        bridge-access 123
        mstpctl-bpduguard yes
        mstpctl-portbpdufilter yes
        vxlan-id 100123
```
```
cumulus@leaf04:mgmt-vrf:~$ ifquery vni123
auto vni123
iface vni123
        bridge-access 123
        mstpctl-bpduguard yes
        mstpctl-portbpdufilter yes
        vxlan-id 123
```

***When ready, continue to the next page to see the solution for this challenge.***
<!-- AIR:page -->

#### Solution
The VXLAN ID configured for the VNI on leaf04 is incorrect. Since the VXLAN ID cannot be modified on an active VNI, we must first bring down the VNI:

```
cumulus@leaf04:mgmt-vrf:~$ sudo ifdown vni123
```

Next, we correct the VXLAN ID on the VNI:

```
cumulus@leaf04:mgmt-vrf:~$ net add vxlan vni123 vxlan id 100123
cumulus@leaf04:mgmt-vrf:~$ net commit
--- /etc/network/interfaces     2020-05-27 20:05:48.840313000 +0000
+++ /run/nclu/ifupdown2/interfaces.tmp  2020-05-27 20:06:13.415481966 +0000
@@ -34,12 +34,12 @@
 auto mgmt
 iface mgmt
   address 127.0.0.1/8
   vrf-table auto
 
 auto vni123
 iface vni123
     bridge-access 123
     mstpctl-bpduguard yes
     mstpctl-portbpdufilter yes
-    vxlan-id 123
+    vxlan-id 100123
 



net add/del commands since the last "net commit"
================================================

User     Timestamp                   Command
-------  --------------------------  ------------------------------------
cumulus  2020-05-27 20:06:10.832097  net add vxlan vni123 vxlan id 100123
```
The VNI is brought back up automatically when the commit is issued.

***Documentation**: [https://docs.cumulusnetworks.com/cumulus-linux-37/Network-Virtualization/Static-VXLAN-Configurations/Static-VXLAN-Tunnels/#requirements](https://docs.cumulusnetworks.com/cumulus-linux-37/Network-Virtualization/Static-VXLAN-Configurations/Static-VXLAN-Tunnels/#requirements)*

*[Continue to next challenge](#challenge-4) or [Back to Lab Overview](#challenge-list)*
<!-- AIR:page -->

### Challenge 4
#### Setup Command
```
./run -c 4 -a load
```

#### Validation Command
```
./run -c 4 -a validate
```

#### Topology Diagram
![topology](https://gitlab.com/cumulus-consulting/education/cumulus-challenge-labs/-/raw/trunk/challenges/4/topology.png)

#### Problem Statement
Servers in rack1 can reach each other. Servers in rack2 can reach each other. Some connectivity between servers attached to the borders and rack1 and rack2 servers is present. Servers in rack1 and rack2 are unable to communicate with each other.

Try to identify the problem in the network leading to the connectivity issues.

***Continue to the next page for hints to help spot the problem.***
<!-- AIR:page -->

#### Hint 1
Check the routing tables on the leaf switches:
```
cumulus@leaf01:mgmt-vrf:~$ net show route vrf RED
show ip route vrf RED 
======================
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route


VRF RED:
K * 0.0.0.0/0 [255/8192] unreachable (ICMP unreachable), 00:08:31
B>* 10.100.1.0/24 [20/0] via 10.0.1.254, vlan4001 onlink, 00:08:18
C * 10.101.1.0/24 is directly connected, vlan101-v0, 00:08:26
C>* 10.101.1.0/24 is directly connected, vlan101, 00:08:26
```
```
cumulus@leaf01:mgmt-vrf:~$ net show route vrf BLUE
show ip route vrf BLUE 
=======================
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route


VRF BLUE:
K * 0.0.0.0/0 [255/8192] unreachable (ICMP unreachable), 00:09:13
B>* 10.200.1.0/24 [20/0] via 10.0.1.254, vlan4002 onlink, 00:09:00
C * 10.201.1.0/24 is directly connected, vlan201-v0, 00:09:08
C>* 10.201.1.0/24 is directly connected, vlan201, 00:09:08
B>* 10.202.1.0/24 [20/0] via 10.0.1.2, vlan4002 onlink, 00:09:08
```
<!-- AIR:page -->

#### Hint 2
Check the EVPN BGP table on the spine switches for the type 5 from rack1 and rack2:
```
cumulus@spine01:mgmt-vrf:~$ net show bgp l2vpn evpn route
BGP table version is 1, local router ID is 10.10.10.101
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal
Origin codes: i - IGP, e - EGP, ? - incomplete
EVPN type-2 prefix: [2]:[ESI]:[EthTag]:[MAClen]:[MAC]:[IPlen]:[IP]
EVPN type-3 prefix: [3]:[EthTag]:[IPlen]:[OrigIP]
EVPN type-5 prefix: [5]:[ESI]:[EthTag]:[IPlen]:[IP]

   Network          Next Hop            Metric LocPrf Weight Path
                    Extended Community
Route Distinguisher: 10.10.10.1:2
*> [5]:[0]:[0]:[24]:[10.101.1.0]
                    10.0.1.1                 0             0 65101 ?
                    RT:65101:304001 ET:8 Rmac:44:38:39:ff:01:01
Route Distinguisher: 10.10.10.1:3
*> [5]:[0]:[0]:[24]:[10.201.1.0]
                    10.0.1.1                 0             0 65101 ?
                    RT:65101:304002 ET:8 Rmac:44:38:39:ff:01:01
Route Distinguisher: 10.10.10.2:2
*> [5]:[0]:[0]:[24]:[10.101.1.0]
                    10.0.1.1                 0             0 65102 ?
                    RT:65102:304001 ET:8 Rmac:44:38:39:ff:01:01
Route Distinguisher: 10.10.10.2:3
*> [5]:[0]:[0]:[24]:[10.201.1.0]
                    10.0.1.1                 0             0 65102 ?
                    RT:65102:304002 ET:8 Rmac:44:38:39:ff:01:01
Route Distinguisher: 10.100.1.2:2
*> [5]:[0]:[0]:[24]:[10.100.1.0]
                    10.0.1.254               0             0 65163 ?
                    RT:65163:304001 ET:8 Rmac:44:38:39:00:01:ff
Route Distinguisher: 10.100.1.3:2
*> [5]:[0]:[0]:[24]:[10.100.1.0]
                    10.0.1.254               0             0 65164 ?
                    RT:65164:304001 ET:8 Rmac:44:38:39:00:01:ff
Route Distinguisher: 10.102.1.2:2
*> [5]:[0]:[0]:[24]:[10.102.1.0]
                    10.0.1.2                 0             0 65103 ?
                    RT:65103:304001 ET:8 Rmac:44:38:39:ff:01:01
Route Distinguisher: 10.102.1.3:2
*> [5]:[0]:[0]:[24]:[10.102.1.0]
                    10.0.1.2                 0             0 65104 ?
                    RT:65104:304001 ET:8 Rmac:44:38:39:ff:01:01
Route Distinguisher: 10.200.1.2:3
*> [5]:[0]:[0]:[24]:[10.200.1.0]
                    10.0.1.254               0             0 65163 ?
                    RT:65163:304002 ET:8 Rmac:44:38:39:00:01:ff
Route Distinguisher: 10.200.1.3:3
*> [5]:[0]:[0]:[24]:[10.200.1.0]
                    10.0.1.254               0             0 65164 ?
                    RT:65164:304002 ET:8 Rmac:44:38:39:00:01:ff
Route Distinguisher: 10.202.1.2:3
*> [5]:[0]:[0]:[24]:[10.202.1.0]
                    10.0.1.2                 0             0 65103 ?
                    RT:65103:304002 ET:8 Rmac:44:38:39:ff:01:02
Route Distinguisher: 10.202.1.3:3
*> [5]:[0]:[0]:[24]:[10.202.1.0]
                    10.0.1.2                 0             0 65104 ?
                    RT:65104:304002 ET:8 Rmac:44:38:39:ff:01:02

Displayed 12 prefixes (12 paths)
```
<!-- AIR:page -->

#### Hint 3
Specifically look at the details of the type 5 from rack1:
```
Route Distinguisher: 10.10.10.1:2
*> [5]:[0]:[0]:[24]:[10.101.1.0]
                    10.0.1.1                 0             0 65101 ?
                    RT:65101:304001 ET:8 Rmac:44:38:39:ff:01:01
Route Distinguisher: 10.10.10.2:2
*> [5]:[0]:[0]:[24]:[10.101.1.0]
                    10.0.1.1                 0             0 65102 ?
                    RT:65102:304001 ET:8 Rmac:44:38:39:ff:01:01
```
Versus the details of the type 5 from rack2:
```
Route Distinguisher: 10.102.1.2:2
*> [5]:[0]:[0]:[24]:[10.102.1.0]
                    10.0.1.2                 0             0 65103 ?
                    RT:65103:304001 ET:8 Rmac:44:38:39:ff:01:01
Route Distinguisher: 10.102.1.3:2
*> [5]:[0]:[0]:[24]:[10.102.1.0]
                    10.0.1.2                 0             0 65104 ?
                    RT:65104:304001 ET:8 Rmac:44:38:39:ff:01:01
```
<!-- AIR:page -->

#### Hint 4
Compare the configured hwaddress on the VLANs for the L3VNIs on the rack2 leaf switches:
```
cumulus@leaf03:mgmt-vrf:~$ ifquery vlan4001
auto vlan4001
iface vlan4001
        hwaddress 44:38:39:ff:01:01
        vlan-id 4001
        vlan-raw-device bridge
        vrf RED
```
Versus the details of the type 5 from rack2:
```
cumulus@leaf03:mgmt-vrf:~$ ifquery vlan4002
auto vlan4002
iface vlan4002
        hwaddress 44:38:39:ff:01:02
        vlan-id 4002
        vlan-raw-device bridge
        vrf BLUE
```
<!-- AIR:page -->

#### Hint 5
Compare the configured hwaddress on the VLANs for the L3VNIs on the rack1 leaf switches:
```
cumulus@leaf01:mgmt-vrf:~$ ifquery vlan4001
auto vlan4001
iface vlan4001
        hwaddress 44:38:39:ff:01:01
        vlan-id 4001
        vlan-raw-device bridge
        vrf RED
```
Versus the details of the type 5 from rack2:
```
cumulus@leaf01:mgmt-vrf:~$ ifquery vlan4002
auto vlan4002
iface vlan4002
        hwaddress 44:38:39:ff:01:01
        vlan-id 4002
        vlan-raw-device bridge
        vrf BLUE
```

***When ready, continue to the next page to see the solution for this challenge.***
<!-- AIR:page -->

#### Solution
The hwaddress is misconfigured on the VLAN associated with the L3VNI for VRF RED on the leaf03/04 pair, and matches the hwaddress configured on the leaf01/02 pair.

Correct the hwaddress assigned to VLAN 4001 on leaf03/04:

```
cumulus@leaf03:mgmt-vrf:~$ net add vlan 4001 hwaddress 44:38:39:ff:01:02
cumulus@leaf03:mgmt-vrf:~$ net commit
```
```
cumulus@leaf04:mgmt-vrf:~$ net add vlan 4001 hwaddress 44:38:39:ff:01:02
cumulus@leaf04:mgmt-vrf:~$ net commit
```
To apply the changes, and flush the previous bad RMAC information, we also need to clear BGP on the leaf and border switches. We can do this easily from the oob-mgmt-server using an Ansible ad-hoc command:

```
cumulus@oob-mgmt-server:~/cumulus-challenge-labs$ ansible -m command -a "net clear bgp all" leaf01,leaf02,leaf03,leaf04,border01,border02
```

***Documentation**: [https://docs.cumulusnetworks.com/cumulus-linux-37/Network-Virtualization/Ethernet-Virtual-Private-Network-EVPN/#svi-for-the-layer-3-vni](https://docs.cumulusnetworks.com/cumulus-linux-37/Network-Virtualization/Ethernet-Virtual-Private-Network-EVPN/#svi-for-the-layer-3-vni)*

[Back to Lab Overview](#challenge-list)
<!-- AIR:tour -->

### Contributing
If you'd like to contribute, please see the [Contributing Guide](CONTRIBUTING.md).